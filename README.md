# Time Clock
This is a small app which is designed to keep track of time. It uses a stack of
activities to track things which are currently happening.

## Usage
Using Time Clock is simple. Time Clock installs as `cl` (short for 'clock'). It
automatically records the time at which you push and pop activities. See a
typical workflow below:

```sh
cl push work
cl push project
cl push lunch
cl pop # lunch
cl pop # project
cl push other-project
cl pop # other-project
cl pop # work
```

### Help Text
```text
$ cl help
Time Clock 0.1.0
Dennis Bellinger (butitsnotme) <dennis.bellinger@gmail.com>
Track time using a stack of activities

USAGE:
    cl [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    help    Prints this message or the help of the given subcommand(s)
    pop     Pop an activity from the stack and record it as finished
    push    Push an activity onto the stack
$ cl help help
cl-help 
Prints this message or the help of the given subcommand(s)

USAGE:
    cl help [subcommand]...

ARGS:
    <subcommand>...    The subcommand whose help message to display
$ cl help pop
cl-pop 
Pop an activity from the stack and record it as finished

USAGE:
    cl pop

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
$ cl help push
cl-push 
Push an activity onto the stack

USAGE:
    cl push <activity>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

ARGS:
    <activity>    The activity to push onto the stack
$ 
```

## Installation

### From Source
Time Clock is written in the Rust Programming Language, so you'll need cargo
installed. You'll also need the SQLite3 development files.

```sh
git clone <this repo>
cd <cloned directory>
cargo install
```
