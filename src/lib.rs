#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

pub mod models;
pub mod reports;
pub mod schema;
pub mod util;

use self::models::*;
use anyhow::{Context, Result};
use chrono::prelude::*;
use chrono::Duration;
use cli_table::format::{CellFormat, Justify, NO_BORDER_COLUMN_TITLE};
use cli_table::{Cell, Row};
use diesel::prelude::*;
#[cfg(not(debug_assertions))]
use directories::UserDirs;
use std::cmp::Ordering;

embed_migrations!();

pub struct Stack {
    conn: SqliteConnection,
    now: DateTime<Local>,
}

impl Default for Stack {
    fn default() -> Stack {
        #[cfg(not(debug_assertions))]
        let database_url = UserDirs::new()
            .map(|dirs| dirs.home_dir().to_path_buf())
            .map(|path| path.join(".time_clock.db").to_string_lossy().into_owned())
            .unwrap_or(".time_clock.db".into());
        #[cfg(debug_assertions)]
        let database_url = String::from(".time_clock.db");

        let conn = SqliteConnection::establish(&database_url)
            .unwrap_or_else(|_| panic!("Error connecting to {}", database_url));

        Stack {
            conn,
            now: Local::now(),
        }
    }
}

impl Stack {
    pub fn new_with_time(t: DateTime<Local>) -> Stack {
        let mut stack: Stack = Default::default();
        stack.now = t;
        stack
    }

    pub fn push_to_stack<'a, I>(&self, names: I) -> Result<()>
    where
        I: Iterator<Item = &'a str>,
    {
        let entries = names
            .map(|name| NewStackEntry {
                time: self.now.timestamp(),
                activity: name.to_string(),
            })
            .collect::<Vec<NewStackEntry>>();

        self.put_stack(&entries)?;
        self.print_current_status()
    }

    pub fn pop_from_stack(&self) -> Result<()> {
        let entries = self.get_top_of_stack()?;
        let activities = entries
            .iter()
            .map(|se| NewActivity {
                start_time: se.time,
                end_time: self.now.timestamp(),
                name: se.activity.clone(),
            })
            .collect::<Vec<_>>();

        self.put_activities(&activities)?;
        self.delete_stack(&entries)?;
        self.print_current_status()?;
        Ok(())
    }

    pub fn drop_from_stack(&self) -> Result<()> {
        let entries = self.get_top_of_stack()?;
        self.delete_stack(&entries)?;
        self.print_current_status()?;
        Ok(())
    }

    pub fn detail_today(&self) -> Result<()> {
        self.print_current_status()
    }

    pub fn migrate(&self) -> Result<&Self> {
        embedded_migrations::run(&self.conn).context("Could not migrate/create database")?;
        Ok(self)
    }
}

impl Stack {
    fn print_current_status(&self) -> Result<()> {
        let time = Duration::seconds(self.now.time().num_seconds_from_midnight() as i64);
        let start = self.now - time;
        let end = start + Duration::hours(24);

        let justify_right = CellFormat::builder().justify(Justify::Right).build();
        let default: CellFormat = Default::default();

        let stack = self.get_full_stack()?;
        let activities = self.get_activities(start.with_timezone(&Utc), end.with_timezone(&Utc))?;

        let headers = Row::new(vec![
            Cell::new("Activity", justify_right),
            Cell::new("Start Time", default),
            Cell::new("End Time", default),
            Cell::new("Duration", default),
        ]);
        let mut activities: Vec<_> = stack
            .into_iter()
            .map::<reports::Activity, _>(|s| s.into())
            .chain(activities.into_iter().map(|a| a.into()))
            .collect();
        activities.sort_by(|a, b| a.partial_cmp(b).unwrap_or(Ordering::Equal).reverse());
        let mut rows: Vec<Row> = activities.into_iter().map(|a| a.into()).collect();
        rows.insert(0, headers);
        cli_table::Table::new(rows, NO_BORDER_COLUMN_TITLE)
            .context("Could not build table")?
            .print_stdout()
            .context("Could not print table")?;

        Ok(())
    }
}

impl Stack {
    fn get_top_of_stack(&self) -> Result<Vec<StackEntry>> {
        use crate::schema::stack::dsl::*;

        stack
            .order_by(id.desc())
            .limit(1)
            .load::<StackEntry>(&self.conn)
            .context("Error reading stack")
    }

    fn get_full_stack(&self) -> Result<Vec<StackEntry>> {
        use crate::schema::stack::dsl::*;

        stack
            .order_by(id.asc())
            .load::<StackEntry>(&self.conn)
            .context("Error reading stack")
    }

    fn put_stack(&self, entries: &[NewStackEntry]) -> Result<()> {
        use crate::schema::stack;
        diesel::insert_into(stack::table)
            .values(entries)
            .execute(&self.conn)
            .map(|_| ())
            .context("Could not save stack entry")
    }

    fn delete_stack(&self, entries: &[StackEntry]) -> Result<()> {
        use crate::schema::stack::dsl::*;

        for entry in entries {
            diesel::delete(stack.filter(id.eq(&entry.id)))
                .execute(&self.conn)
                .context("Could not delete stack entries")?;
        }

        Ok(())
    }

    fn put_activities(&self, acts: &[NewActivity]) -> Result<()> {
        use crate::schema::activities;
        diesel::insert_into(activities::table)
            .values(acts)
            .execute(&self.conn)
            .map(|_| ())
            .context("Could not save stack entries")
    }

    fn get_activities(&self, start: DateTime<Utc>, end: DateTime<Utc>) -> Result<Vec<Activity>> {
        use crate::schema::activities::dsl::*;
        activities
            .filter(end_time.gt(start.timestamp()))
            .filter(start_time.lt(end.timestamp()))
            .order_by(start_time.desc())
            .load::<Activity>(&self.conn)
            .context("Could not load activities")
    }
}
