use super::schema::{activities, stack};

#[derive(Insertable)]
#[table_name = "stack"]
pub struct NewStackEntry {
    pub time: i64,
    pub activity: String,
}

#[derive(Insertable)]
#[table_name = "activities"]
pub struct NewActivity {
    pub start_time: i64,
    pub end_time: i64,
    pub name: String,
}

#[derive(Queryable)]
pub struct StackEntry {
    pub id: i32,
    pub time: i64,
    pub activity: String,
}

#[derive(Queryable)]
pub struct Activity {
    pub id: i32,
    pub start_time: i64,
    pub end_time: i64,
    pub name: String,
}
