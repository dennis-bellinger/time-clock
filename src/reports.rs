use crate::models::StackEntry;
use crate::util::DurationExt;
use chrono::{DateTime, Local, TimeZone};
use cli_table::format::{CellFormat, Justify};
use cli_table::{Cell, Row};
use std::cmp::Ordering;

#[derive(Debug, Clone, PartialEq)]
pub struct Activity {
    pub start_time: DateTime<Local>,
    pub end_time: Option<DateTime<Local>>,
    pub name: String,
}

impl From<super::models::Activity> for Activity {
    fn from(orig: super::models::Activity) -> Self {
        Activity {
            start_time: Local.timestamp(orig.start_time, 0),
            end_time: Some(Local.timestamp(orig.end_time, 0)),
            name: orig.name,
        }
    }
}

impl From<super::models::StackEntry> for Activity {
    fn from(orig: StackEntry) -> Self {
        Activity {
            start_time: Local.timestamp(orig.time, 0),
            end_time: None,
            name: orig.activity,
        }
    }
}

impl From<Activity> for Row {
    fn from(a: Activity) -> Self {
        let justify_right = CellFormat::builder().justify(Justify::Right).build();
        let default: CellFormat = Default::default();

        Row::new(vec![
            Cell::new(&a.name, justify_right),
            Cell::new(&a.start_time.format("%Y-%m-%d %H:%M:%S"), default),
            Cell::new(
                &a.end_time
                    .map(|t| t.format("%Y-%m-%d %H:%M:%S"))
                    .map(|f| format!("{}", f))
                    .unwrap_or("".into()),
                default,
            ),
            Cell::new(
                &(a.end_time.unwrap_or(Local::now()) - a.start_time).pretty(),
                default,
            ),
        ])
    }
}

impl PartialOrd for Activity {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let ord = self.start_time.partial_cmp(&other.start_time);

        let ord = match ord {
            Some(Ordering::Equal) => {
                if self.end_time.is_some() && other.end_time.is_some() {
                    self.end_time.unwrap().partial_cmp(&other.end_time.unwrap())
                } else if self.end_time.is_some() {
                    Some(Ordering::Less)
                } else {
                    Some(Ordering::Greater)
                }
            }
            ord => ord,
        };

        match ord {
            Some(Ordering::Equal) => self.name.partial_cmp(&other.name),
            ord => ord,
        }
    }
}
