use chrono::Duration;

pub trait DurationExt {
    fn pretty(&self) -> String;
}

impl DurationExt for Duration {
    fn pretty(&self) -> String {
        let weeks = self.num_weeks();
        let days = self.num_days() - self.num_weeks() * 7;
        let hours = self.num_hours() - self.num_days() * 24;
        let minutes = self.num_minutes() - self.num_hours() * 60;
        let seconds = self.num_seconds() - self.num_minutes() * 60;

        let mut out = String::new();

        if 0 < weeks {
            out += &*format!("{}w", weeks);
        }

        if 0 < days {
            out += &*format!("{}d", days);
        }

        if 0 < hours {
            out += &*format!("{}h", hours);
        }

        if 0 < minutes {
            out += &*format!("{}m", minutes);
        }

        if 0 < seconds {
            out += &*format!("{}s", seconds);
        }

        out
    }
}
