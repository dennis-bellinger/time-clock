extern crate clap;
extern crate time_clock;

use self::time_clock::*;
use anyhow::{Context, Result};
use chrono::Local;
use chrono_english::{parse_date_string, Dialect};
use clap::{App, Arg, SubCommand};

fn main() -> Result<()> {
    let matches = App::new("Time Clock")
        .version("0.1.0")
        .author("Dennis Bellinger (butitsnotme) <dennis.bellinger@gmail.com>")
        .about("Track time using a stack of activities")
        .subcommand(
            SubCommand::with_name("push")
                .about("Push an activity onto the stack")
                .arg(
                    Arg::with_name("activity")
                        .required(true)
                        .help("The activity to push onto the stack"),
                )
                .arg(
                    Arg::with_name("time")
                        .help("Set an alternate time for 'now'")
                        .long("time")
                        .takes_value(true),
                ),
        )
        .subcommand(
            SubCommand::with_name("pop")
                .about("Pop an activity from the stack and record it as finished")
                .arg(
                    Arg::with_name("time")
                        .help("Set an alternate time for 'now'")
                        .long("time")
                        .takes_value(true),
                ),
        )
        .subcommand(
            SubCommand::with_name("list")
                .about("List the time spent today")
                .arg(
                    Arg::with_name("time")
                        .help("Set an alternate time for 'now'")
                        .long("time")
                        .takes_value(true),
                ),
        )
        .subcommand(
            SubCommand::with_name("drop")
                .about("Pop an activity from the stack and discard it")
                .arg(
                    Arg::with_name("time")
                        .help("Set an alternate time for 'now'")
                        .long("time")
                        .takes_value(true),
                ),
        )
        .get_matches();

    if let Some(cmd) = matches.subcommand_matches("push") {
        let items = vec![cmd.value_of("activity").context("No Activity")?];
        let now = if let Some(time) = cmd.value_of("time") {
            parse_date_string(time, Local::now(), Dialect::Uk)?
        } else {
            Local::now()
        };
        Stack::new_with_time(now)
            .migrate()?
            .push_to_stack(items.into_iter())?;
    } else if let Some(cmd) = matches.subcommand_matches("pop") {
        let now = if let Some(time) = cmd.value_of("time") {
            parse_date_string(time, Local::now(), Dialect::Uk)?
        } else {
            Local::now()
        };
        Stack::new_with_time(now).migrate()?.pop_from_stack()?;
    } else if let Some(cmd) = matches.subcommand_matches("list") {
        let now = if let Some(time) = cmd.value_of("time") {
            parse_date_string(time, Local::now(), Dialect::Uk)?
        } else {
            Local::now()
        };
        Stack::new_with_time(now).migrate()?.detail_today()?;
    } else if let Some(cmd) = matches.subcommand_matches("drop") {
        let now = if let Some(time) = cmd.value_of("time") {
            parse_date_string(time, Local::now(), Dialect::Uk)?
        } else {
            Local::now()
        };
        Stack::new_with_time(now).migrate()?.drop_from_stack()?;
    } else {
        println!("{}", matches.usage());
    }

    Ok(())
}
