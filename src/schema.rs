table! {
    activities (id) {
        id -> Integer,
        start_time -> BigInt,
        end_time -> BigInt,
        name -> Text,
    }
}

table! {
    stack (id) {
        id -> Integer,
        time -> BigInt,
        activity -> Text,
    }
}

allow_tables_to_appear_in_same_query!(activities, stack,);
