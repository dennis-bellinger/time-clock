-- Your SQL goes here
CREATE TABLE stack (
    id INTEGER PRIMARY KEY NOT NULL,
    time BIG INT NOT NULL,
    activity TEXT NOT NULL,
    UNIQUE (activity)
);

CREATE TABLE activities (
    id INTEGER PRIMARY KEY NOT NULL,
    start_time BIG INT NOT NULL,
    end_time BIG INT NOT NULL,
    name TEXT NOT NULL,
    CHECK (start_time < end_time)
);

CREATE INDEX time ON stack (time);
CREATE INDEX start_end ON activities (start_time, end_time);

