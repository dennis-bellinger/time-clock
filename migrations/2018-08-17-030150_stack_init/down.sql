-- This file should undo anything in `up.sql`

DROP INDEX time;
DROP INDEX start_end;

DROP TABLE stack;
DROP TABLE activities;
